#
# Latest Gentoo, patched, with MariaDB
#
FROM iqhive/gentoo-patched
MAINTAINER Relihan Myburgh <rmyburgh@iqhive.com>

RUN emerge -v dev-db/mariadb; rm /usr/portage/distfiles/*
